import React, {Fragment,useState}from 'react';
import uuid from 'uuid/v4';
const Formulario = ({crearVoluntario}) => {
// State de voluntario
const [voluntario, actualizarVoluntario]=useState({
    nombre:'',
    apellido:'',
    fecha:'',
    hora:'',
    funcion:'',
});

//state de error
const [error,actualizarError]=useState(false);
// Funcion que se ejecuta cada vez que el usuario escribe en un input
const actualizarState =(e)=>{
    actualizarVoluntario ({
        ...voluntario,
        [e.target.name] : e.target.value
})
}
//extraer valores
const {nombre,apellido,fecha,hora,funcion}=voluntario;

// Cuando el usuario preciona agregar voluntario
const submitVoluntario = (e)=>{
    e.preventDefault();

    //Validación del form
     if(nombre.trim()==='' || apellido.trim()==='' ||fecha.trim()==='' || hora.trim()==='' || funcion.trim()===''){
         actualizarError(true);
         console.log('campo vacio');
         return;
     }
     
     //Eliminar mensaje previo
     actualizarError(false); 
console.log('agregando')

//ASIGNAR UN ID: instalamos una libreria llamada UUid, EN LA CONSOLA COLOCAMOS npm i uuid
voluntario.id=uuid();

//Crear voluntario
crearVoluntario(voluntario);

//Reiniciar Form
actualizarVoluntario({
    nombre:'',
    apellido:'',
    fecha:'',
    hora:'',
    funcion:'',
})

}


    return ( 
        <Fragment>
            <h2>Registrar Voluntario</h2>
            <form 
            onSubmit={submitVoluntario}
            data-netlify="true"
            method="POST"
            >
            {error
            ? <p className="alerta-error">Todos los campos son necesarios</p>
            :null
            
            }
              <label>Nombre</label>
              <input
                 type="text"
                 name="nombre"
                 className="u-full-width"
                 placeholder="Ejemplo: David"
                 onChange={actualizarState}
                 value={nombre}
              />

              <label>Apellido</label>
              <input
                 type="text"
                 name="apellido"
                 className="u-full-width"
                 placeholder="Ejemplo: González"
                 onChange={actualizarState}
                 value={apellido}
              />
             <label>Fecha:</label>
              <input
                 type="date"
                 name="fecha"
                 className="u-full-width"
                 onChange={actualizarState}
                 value={fecha}
              />

               <label>Horario:</label>
              <select
              
                 name="hora"
                 className="u-full-width"
                 onChange={actualizarState}
                 value={hora}
              >
                  <option>seleccionar</option>
                  <option>11:30</option>
                  <option>17:30</option>
                  <option>20:00</option>
                </select>

              <label>Función</label>
              <textarea
              className="u-full-width"
              name="funcion"
              placeholder="Ejemplo: Dar la Bienvenida"
              onChange={actualizarState}
              value={funcion}
              ></textarea>
             <div className="field">
                 <div data-netlify-recaptcha="true"></div>
             </div>
              <button
              type="submit"
              className="u-full-width agregarV button-primary"
              
              >Agregar Voluntario</button>

              
            </form>

        </Fragment>
     );
}
 
export default Formulario;