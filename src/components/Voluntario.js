import React from 'react';
const Voluntario = ({voluntario,eliminarVoluntario}) => {
    return ( 

        <div className="cita">
            <p>Nombre: <span>{voluntario.nombre}</span></p>
            <p>Apellido: <span>{voluntario.apellido}</span></p>
            <p>Fecha: <span>{voluntario.fecha}</span></p>
            <p>Hora: <span>{voluntario.hora}</span></p>
            <p>Función: <span>{voluntario.funcion}</span></p>
            <button
            type="button"
            className="button eliminar u-full-width"
            onClick={()=>eliminarVoluntario(voluntario.id)}
            
            > &times; Quitar Voluntario</button>
        </div> 
     );
}
 
export default Voluntario;