import React, {Fragment,useState,useEffect} from 'react';
import Formulario from './components/Formulario';
import Voluntario from './components/Voluntario';
import hillsong from './hillsong.png';


function App() {


//agragar voluntarios en el local storage
let voluntariosIniciales = JSON.parse(localStorage.getItem('voluntarios'));
if(!voluntariosIniciales){
  voluntariosIniciales = [];
}

  //Arreglo de voluntarios
const [voluntarios,guardarVoluntarios]= useState(voluntariosIniciales);

// use effect para realizar ciertas operaciones cuando el state cambia
useEffect( ()=>{
  if(voluntariosIniciales){
    localStorage.setItem('voluntarios', JSON.stringify(voluntarios))
  }
  else{
    localStorage.setItem('voluntarios', JSON.stringify([]));
  }
}, [voluntarios]);

//Funcion que tome la voluntariosvoluntario actuales y guarde la nueva
const crearVoluntario = voluntario=>{
  guardarVoluntarios([...voluntarios, voluntario]);
}

//Funcion para eliminar la voluntario por su id
const eliminarVoluntario = id =>{
  const nuevosVoluntarios = voluntarios.filter(voluntario => voluntario.id !==id);
  guardarVoluntarios (nuevosVoluntarios);
}

//condicional que cambia el titulo
const titulo = voluntarios.length===0 ?'Sin Voluntarios' :'Voluntarios Registrados' ; 
  return (
   <Fragment>
     <div className="header">
       <center>
     <img src={hillsong}/>
     <h1>Administrador de Voluntarios</h1>
     </center>
     </div>
     <div className="container">
       <div className="row">
         <div className="one-half column contenedor">
           <Formulario
           crearVoluntario={crearVoluntario}
           />
         </div>
         <div className="one-half column contenedor">
           <h2>{titulo}</h2>
           {voluntarios.map(voluntario=>(
             <Voluntario
             key={voluntarios.id}
             voluntario={voluntario}
             eliminarVoluntario={eliminarVoluntario}
             />
             ))}
             
          </div>
       </div>
     </div>
   </Fragment>
  );
}

export default App;

